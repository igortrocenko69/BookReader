package book_reader;

/**
 * Created by Igor on 15.09.17.
 */
 class TwoTuple<A, B> {
     final A first;
     final B second;

    TwoTuple(A a, B b) {
        first = a;
        second = b;
    }
}
