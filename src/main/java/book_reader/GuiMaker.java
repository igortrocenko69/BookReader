package book_reader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Igor on 06.09.17.
 * Create the GUI and show it
 */
public class GuiMaker {
    private String strURL;
    private Task bookReader;
    private JFrame frame;
    //final String url = "http://loveread.ec/read_book.php?id=25166&p=1";

    private GuiMaker() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        frame = new JFrame("Загрузка книги с сайта");
        frame.setLayout(new FlowLayout());
        //frame.setLayout(new GridLayout(0,1));
        frame.setSize(400, 200);
        frame.setLocation(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel URLLabel = new JLabel("Введите адрес сайта :");
        JTextField UrlTextField = new JTextField(30);
        UrlTextField.setForeground(Color.darkGray);
        //UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"); //подключаемые стили оформления
        //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        //UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
        UrlTextField.setToolTipText("адрес сайта");

        JLabel progressLabel = new JLabel("Идет загрузка...     ");
        JProgressBar progressBar = new JProgressBar(0, 100);

        //progressBar.setIndeterminate(true);
        //frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)); курсор в режиме ожидания
        // progressBar.setStringPainted(true);
        //progressBar.setMinimum(0);
        //progressBar.setMaximum(100);

        JButton submit = new JButton("Принять");
        GuiMaker maker = this;


        submit.addActionListener(new ActionListener() {
            /**
             * Invoked when the user presses the submit batton
             *
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if (UrlTextField.getText().equals("")) {
                    JOptionPane.showMessageDialog(frame, "Не введен адрес сайта", "Ошибка", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                submit.setEnabled(false);
                frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                strURL = UrlTextField.getText();

                bookReader = new Task(strURL, submit, frame, maker);                //запуск задачи на выполнение
                bookReader.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {          //обработчик состояния progressBar
                        if ("progress" == evt.getPropertyName()) {
                            int progress = (Integer) evt.getNewValue();
                            progressBar.setValue(progress);
                            //taskOutput.append(String.format(
                            //        "Completed %d%% of task.\n", task.getProgress()));
                        }
                    }
                });
                bookReader.execute();
            }
        });
        frame.add(URLLabel);
        frame.add(UrlTextField);
        frame.add(progressLabel);
        frame.add(progressBar);
        frame.add(submit);
        frame.setVisible(true);
    }

    /**
     * Method showing Dialog for recording to hard disc
     *
     * @param content-filename and text
     */
    void showSaveToFileDialog(TwoTuple<String, String> content) {
        int result = JOptionPane.showConfirmDialog(frame, "Вы хотите сохранить файл?", "Окно подтверждения сохранения", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Сохранение файла");
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setCurrentDirectory(new File("C:\\Users\\Ihor\\Desktop\\Книги"));
            //fileName = bookReader.getFileName();
            fileChooser.setSelectedFile(new File(content.second + ".txt"));
            result = fileChooser.showSaveDialog(frame);
            if (result == JFileChooser.APPROVE_OPTION) {
                // File dir=fileChooser.getCurrentDirectory();
                File selectedFile = fileChooser.getSelectedFile();
                // String path=dir.getPath()+selectedFile.getPath();
                // String path=selectedFile.getPath();
                saveToFile(content.first, selectedFile, frame);

            }

        } else if (result == JOptionPane.CANCEL_OPTION) {
            result = JOptionPane.showConfirmDialog(frame, "Вы хотите выйти из программы?", "Окно подтвержения выхода", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (result == JOptionPane.YES_OPTION) {
                System.exit(0);
            } else if (result == JOptionPane.NO_OPTION) {
            }

        }
    }

    /**
     * Method saving text to file
     *
     * @param text-received text
     * @param path-File
     * @param frame-JFrame
     */
    private void saveToFile(String text, File path, JFrame frame) {
        try {
            try (FileWriter fw = new FileWriter(path)) {
                fw.write(text);
                JOptionPane.showMessageDialog(frame, "Книга записана успешно");
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Ошибка записи файла", "Ошибка", JOptionPane.ERROR_MESSAGE);
        }

    }

    public static void main(String[] args) throws IOException {


        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                try {
                    UIManager.put(
                            "FileChooser.saveButtonText", "Сохранить");
                    UIManager.put(
                            "FileChooser.cancelButtonText", "Отмена");
                    UIManager.put(
                            "FileChooser.fileNameLabelText", "Наименование файла");
                    UIManager.put(
                            "FileChooser.filesOfTypeLabelText", "Типы файлов");
                    UIManager.put(
                            "FileChooser.lookInLabelText", "Директория");
                    UIManager.put(
                            "FileChooser.saveInLabelText", "Сохранить в директории");
                    UIManager.put(
                            "FileChooser.folderNameLabelText", "Путь директории");
                    new GuiMaker();
                } catch (ClassNotFoundException | UnsupportedLookAndFeelException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
