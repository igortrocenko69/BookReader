package book_reader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * Created by Igor on 06.09.17.
 * This class parsing http://loveread.ec using Jsoup library.
 */
public class Task extends SwingWorker<TwoTuple<String, String>, Void> {
    private static final int TIMEOUT = 10 * 10000;
    private String url;
    private JButton submit;
    private JFrame frame;
    private GuiMaker maker;

    Task(String url, JButton submit, JFrame frame, GuiMaker maker) {
        this.url = url;
        this.submit = submit;
        this.frame = frame;
        this.maker = maker;
    }

    /**
     *  Main task.Executed in background thread
     */
    @Override
    protected TwoTuple<String, String> doInBackground() throws Exception {

        return readBook(url);

    }



    @Override
    public void done() {
        Toolkit.getDefaultToolkit().beep();
        submit.setEnabled(true);
        frame.setCursor(null);                              //turn off the wait cursor
        try {
            maker.showSaveToFileDialog(get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        //taskOutput.append("Done!\n");
    }

    /**
     * Method receive filename and text content
     *
     * @param url-address of page
     * @return TwoTuple with filename and text
     */
    private TwoTuple<String, String> readBook(String url) throws IOException {

        Document content = downloadFromLink(url);
        String text = getText(content);
        String fileName = getTitle(content);
        setProgress(100);
        return new TwoTuple<String, String>(text, fileName);  //создаю кортеж из текст.данных и имени файла


    }


    private Document downloadFromLink(String link) throws IOException {
        return Jsoup.connect(link)
                .userAgent("Mozilla")
                .timeout(TIMEOUT)
                .get();


    }

    /**
     * Method receive fool text and set progress value
     */
    private String getText(Document doc) throws IOException {

        List<String> textPageLinks = getPageLinks(doc);                       //ссылки на страницы
        String firstPageText = processTextPage(doc);                           //текст с первой страницы
        StringBuilder sb = (new StringBuilder(firstPageText)).append("\r\n");
        int progress = 0;
        setProgress(0);
        Random random = new Random();

        for (String pageLink : textPageLinks) {
            Document pageContent = downloadFromLink(pageLink);
            String textPage = processTextPage(pageContent);
            sb.append(textPage).append("\r\n");                           //текст с остальных страниц

            try {
                Thread.sleep(random.nextInt(500));
            } catch (InterruptedException ignore) {
            }
            progress += 5;
            if (progress >= 100) {
                progress = 0;
            }
            setProgress(progress);


        }

        return sb.toString();
    }

    /**
     * Method receive list of links on text pages
     */
    private List<String> getPageLinks(Document doc) {                     //получаю ссылки на текстовые страницы
        List<String> textPageLinks = new ArrayList<>();
        Elements linkHttpPages = doc.select("div.navigation>a");
        Element lastPageElement = linkHttpPages.get(linkHttpPages.size() - 2);       //єлемент последней страницы
        int lastPageNumber = Integer.valueOf(lastPageElement.text());                //номер последней страницы
        String hrefValue = "http://loveread.ec/" + lastPageElement.attr("href");     //значение аттрибута href
        String linkPattern = hrefValue.substring(0, hrefValue.lastIndexOf("p=") + 2);//шаблон ссылки без номера страницы

        //Elements linkHttpPages = doc.select("#menunav>li>a");
       /* for (Element linkHttpPage : linkHttpPages) {
            if (StringUtils.isNumeric(linkHttpPage.text())) {                   //если есть несколько страниц

                String linkHttpPageStr = "http://loveread.ec/" + linkHttpPage.attr("href");  //получаю ссылки на них
                textPageLinks.add(linkHttpPageStr);                            //и добавляю в коллекцию
            }
        }*/
        for (int i = 2; i <= lastPageNumber; i++) {
            String httpPageLink = linkPattern + i;
            textPageLinks.add(httpPageLink);
        }

        return textPageLinks;
    }

    /**
     * Method receive text page
     */
    private String processTextPage(Document doc) {

        StringBuilder sb = new StringBuilder();
        String chapter = "";
        String chapterDescription = "";

        Element elementChapter = doc.select("div.take_h1").first();
        if (elementChapter != null) {
            chapter = elementChapter.text();
            //sb.append(chapter).append("\n");
        }
        Element elementChapterDescription = doc.select("div.take_h1>i").first();
        if (elementChapterDescription != null) {
            chapterDescription = elementChapterDescription.text();

        }
        sb.append(chapter.substring(0, chapter.lastIndexOf(chapterDescription))).append("\r\n").append(chapterDescription).append("\r\n");


        Elements pageItems = doc.select("div.MsoNormal>p");                  //получаю текстовые абзацы со страницы
        for (Element pageItem : pageItems) {
            String pageItemStr = pageItem.select("p").first().text();
            sb.append(pageItemStr).append("\r\n");                                          //и формирую страницу
        }


        return sb.toString();
    }

    /**
     * Method receive filename for recording
     */

    private String getTitle(Document doc) {

        String str = doc.title();
        StringBuilder sb = new StringBuilder(str);
        sb = new StringBuilder(sb.substring(0, sb.lastIndexOf(" | LoveRead.ec - читать книги онлайн бесплатно")));
        sb.delete(sb.indexOf("|") - 1, sb.indexOf("|") + 1);
        return sb.toString().replace(".", "").replace(" ", "_");

    }


}
